frases = [];

frases.push(new Frase("Si pesa más que un pollo, |me la follo."));
frases.push(new Frase("Si pesa más que un potro, |que se la folle otro."));
frases.push(new Frase("A caballo regalado, |no le mires el diente."));
frases.push(new Frase("En abril, |aguas mil."));
frases.push(new Frase("No por mucho madrugar, |amanece más temprano."));
frases.push(new Frase("A quien madruga, |Dios le ayuda."));
frases.push(new Frase("Hasta el cuarenta de Mayo, |no te quites el sayo."));
frases.push(new Frase("Cuando en Marzo mayea, |en Mayo marcea."));
// frases.push(new Frase("No vendas la piel del oso antes de |cazarla."));
frases.push(new Frase("A perro flaco, |todo son pulgas."));
frases.push(new Frase("Más vale pájaro en mano que |que ciento volando."));
frases.push(new Frase("Quien mucho abarca, |poco aprieta."));
frases.push(new Frase("La avaricia |rompe el saco."));
frases.push(new Frase("Más sabe el diablo por viejo que |por ser diablo."));
frases.push(new Frase("Perro ladrador, |poco mordedor."));
frases.push(new Frase("En el país de los ciegos, |el tuerto es el rey."));
frases.push(new Frase("Aunque la mona se vista de seda, |mona se queda."));
frases.push(new Frase("El que la sigue, |la consigue."));
frases.push(new Frase("A cada cerdo, |le llega su San Martín."));
frases.push(new Frase("Muerto el perro, |se acabó la rabia."));
frases.push(new Frase("Pan con pan, |comida de tontos."));
frases.push(new Frase("De tal palo, |tal astilla."));
frases.push(new Frase("No se hizo la miel para |la boca del asno."));
frases.push(new Frase("Cría cuervos y |te sacarán los ojos."));
frases.push(new Frase("No se puede pedir |peras al olmo."));
frases.push(new Frase("No está el horno |para bollos."));
frases.push(new Frase("Nadie diga: |de esta agua no beberé."));
frases.push(new Frase("Piensa el ladrón que |todos son de su condición."));
frases.push(new Frase("Ojos que no ven, |corazón que no siente."));
frases.push(new Frase("En casa del herrero, |cuchillo de palo."));
frases.push(new Frase("A palabras necias, |oídos sordos."));
frases.push(new Frase("Juan Palomo: |yo me lo guiso, yo me lo como."));
frases.push(new Frase("Por la boca |muere el pez."));
frases.push(new Frase("Cuando el grajo vuela bajo, |hace un frío del carajo."));
frases.push(new Frase("Cuando las barbas de tu vecino veas pelar, |pon las tuyas a remojar."));
frases.push(new Frase("Después de la tempestad, |viene la calma."));
frases.push(new Frase("Dime con quien andas y |te diré quien eres."));
frases.push(new Frase("Si pesa más que un pollo, |dijo la sartén al cazo."));
frases.push(new Frase("A lo hecho, |pecho."));
frases.push(new Frase("Al mal tiempo, |buena cara."));
frases.push(new Frase("Si Mahoma no va a la montaña, |la montaña irá a Mahoma."));
frases.push(new Frase("Quien calla, |otorga."));
frases.push(new Frase("Quien se pica, |ajos come."));
frases.push(new Frase("Mal de muchos, |consuelo de tontos."));
frases.push(new Frase("No se ganó Zamora |en una hora."));
frases.push(new Frase("No sólo de pan |vive el hombre."));
frases.push(new Frase("Más vale solo que |mal acompañado."));
frases.push(new Frase("Más vale prevenir que |curar."));
frases.push(new Frase("La pereza |es la madre de todos los vicios"));
frases.push(new Frase("A falta de pan, |buenas son tortas."));
frases.push(new Frase("Cada oveja, |con su pareja."));
frases.push(new Frase("Cada loco, |con su tema."));
frases.push(new Frase("Donde las dan, |las toman."));
frases.push(new Frase("De los cuarenta para arriba, |no te mojes la barriga."));
frases.push(new Frase("En boca cerrada, |no entran moscas."));
frases.push(new Frase("Hombre prevenido, |vale por dos."));
frases.push(new Frase("Entre col y col, |lechuga."));
frases.push(new Frase("Hablando del rey de Roma, |por la puerta asoma."));
frases.push(new Frase("Éramos pocos y |parió la abuela."));
frases.push(new Frase("Como el perro del hortelano: |ni come ni deja comer."));
frases.push(new Frase("El que no llora, |no mama."));
frases.push(new Frase("El hábito, |no hace al monje."));
frases.push(new Frase("Dios los cría y |ellos se juntan."));
frases.push(new Frase("Cada mochuelo, |a su olivo."));
frases.push(new Frase("Zapatero, |a tus zapatos"));
frases.push(new Frase("Muerto el perro, |se acabó la rabia."));
frases.push(new Frase("Al pan, pan y |al vino, vino."));
frases.push(new Frase("Quien saca molla, |saca polla."));
frases.push(new Frase("Más vale maña que |fuerza."));
frases.push(new Frase("Ojo por ojo, |diente por diente."));
frases.push(new Frase("Sarna con gusto, |no pica."));
frases.push(new Frase("Quien fue a Sevilla, |perdió su silla."));
frases.push(new Frase("Quien fue a Japón, |perdió su sillón."));
frases.push(new Frase("El que primero lo huele, |debajo lo tiene."));
frases.push(new Frase("Monté un circo y |me crecen los enanos."));
frases.push(new Frase("A grandes males, |grandes remedios."));
frases.push(new Frase("A la vejez, |pájaros otra vez."));
frases.push(new Frase("Al mal tiempo, |buena cara."));
frases.push(new Frase("Andando, que |es gerundio."));
frases.push(new Frase("En casa del herrero, |como Pedro por su casa."));
frases.push(new Frase("De oca en oca y |tiro porque me toca."));
frases.push(new Frase("Rebota, rebota y |en tu culo explota."));
frases.push(new Frase("Chincha rabiña, |cara de tortilla."));
frases.push(new Frase("Donde dije digo, |digo Diego."));
frases.push(new Frase("Dos son compañía; |tres son multitud"));
frases.push(new Frase("Donde caben dos, |caben tres."));
frases.push(new Frase("O follamos todos o |la puta al río."));
frases.push(new Frase("Si el río suena, |agua lleva."));
frases.push(new Frase("Una vez al año, |no hace daño."));
frases.push(new Frase("El perro de San Roque no tiene rabo porque |Ramón Ramírez se lo ha cortado."));
frases.push(new Frase("Si el césped ha crecido, |juguemos el partido."));
frases.push(new Frase("Si llega al telefonillo |me la cepillo."));
frases.push(new Frase("Si cumple los trece, |lo merece."));
frases.push(new Frase("Si aún no menstrúa, |puntúa."));
frases.push(new Frase("Si llega al pomo, |barra de lomo."));
frases.push(new Frase("Si se sube a una caja de zapatos sin aplastarla, |espera un año para empalarla."));
frases.push(new Frase("Si aun no sabe hablar, |no podrá declarar. "));
frases.push(new Frase("Si ha pasado la lactancia |ya carece de importancia"));
frases.push(new Frase("Caramelos de limón, |cloroformo y al furgón."));
frases.push(new Frase("Si articula palabra, |se taladra."));
frases.push(new Frase("Por la garra |se conoce al león"));
frases.push(new Frase("El que folla pagando, |acaba ahorrando."));
frases.push(new Frase("Manolete, si no sabes, |para qué te metes"));
frases.push(new Frase("Nunca es tarde si |la dicha es buena"));
frases.push(new Frase("A dios rogando y |con el mazo dando"));
frases.push(new Frase("A grandes males, |grandes remedios"));
frases.push(new Frase("A rio revuleto, |ganancia de pescadores"));
frases.push(new Frase("Cuando el gato no está, |los ratones se divierten"));
frases.push(new Frase("Cuando hay hambre, |no hay pan duro"));
frases.push(new Frase("De noche |todos los gatos son pardos"));
frases.push(new Frase("Dios aprieta pero |no ahorca"));
frases.push(new Frase("La ley es dura, pero |es la ley"));
frases.push(new Frase("Siembra vientos, y |cosecharás tempestades"));
frases.push(new Frase("No todo el campo |es orégano"));
frases.push(new Frase("Hecha la ley, |hecha la trampa"));
frases.push(new Frase("Llorar sobre |la leche derramada"));
frases.push(new Frase("Pan para hoy, y |hambre para mañana"));
frases.push(new Frase("Todos somos inocentes, pero |el poncho no aparece"));
frases.push(new Frase("Si existe |está en Toys'r'us"));
frases.push(new Frase("Cuesta más la correa que |el perro"));

function nuevaFrase() {
	document.getElementById("frase").innerHTML += "- " + crearFrase() + "<p>";
	document.getElementById("frase").scrollTop = document.getElementById("frase").scrollHeight;
}

function Frase(frase) {
	this.ppo = frase.substr(0,frase.indexOf("|"));
	this.fin = frase.substr(frase.indexOf("|") + 1, frase.length - frase.indexOf("|") - 1);
}

function crearFrase(palabra) {
	if(!palabra){
		var frase1 = randomNum(0, frases.length-1);
		do {
			var frase2 = randomNum(0, frases.length-1);
		} while(frase2 == frase1);
		
		return frases[frase1].ppo + frases[frase2].fin;
	}
	else{
		return crearFraseFiltrada(palabra);
	}
	
}

function crearFraseFiltrada(palabra) {
	subfrases_ppo = [];
	subfrases_fin = [];
	var cont = 0;
	var pal_acc = normalizar(palabra).toUpperCase();
	for (let frase of frases){
		if (normalizar(frase.ppo).toUpperCase().includes(pal_acc))
			subfrases_ppo.push([cont, frase.ppo])
		if (normalizar(frase.fin).toUpperCase().includes(pal_acc))
			subfrases_fin.push([cont, frase.fin])
		cont++;
	}
	var res = ""
	if(subfrases_ppo.length && (randomNum(0,1) || !subfrases_fin.length)){
		do{
		var frase1 = randomNum(0, subfrases_ppo.length-1);
		var frase2 = randomNum(0, frases.length-1);
		} while(subfrases_ppo[frase1][0] == frase2)
		res = subfrases_ppo[frase1][1] + frases[frase2].fin;
	}
	else if(subfrases_fin.length){
		do{
			var frase1 = randomNum(0, frases.length-1);
			var frase2 = randomNum(0, subfrases_fin.length-1);
		} while(subfrases_fin[frase2][0] == frase1)
		res = frases[frase1].ppo + subfrases_fin[frase2][1];
	}
	else
		res = crearFrase();
	return res;
}

function normalizar(frase){
	return frase.normalize('NFD').replace(/[\u0300-\u036f]/g, "")
}

function randomNum (min, max) {
	return Math.floor(Math.random() * (max-min+1) + min);
}

try {
	module.exports = crearFrase;
} catch(e) {
	// Do nothing
}

