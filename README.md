# Setup

1. Clone the repo
2. Be sure to have node & npm installed
3. Go to the project's root folder
4. `npm install`
5. Edit .env to add the bot token.

# Test environment

In order to test the bot, you should create a development bot and add it's credentials to .env file. 
To do so, you must talk to BotFather and do de following: 

/newbot

> Alright, a new bot. How are we going to call it? Please choose a name for your bot.

[Pick a name]

> Good. Now let's choose a username for your bot. It must end in `bot`. Like this, for example: TetrisBot or tetris_bot.

[pick the bot's username]

> Done! Congratulations on your new bot. You will find it at t.me/[username]. You can now add a description, about section and profile picture for your bot, see /help for a list of commands. By the way, when you've finished creating your cool bot, ping our Bot Support if you want a better username for it. Just make sure the bot is fully operational before you do this.
> Use this token to access the HTTP API:
> [token]
> For a description of the Bot API, see this page: https://core.telegram.org/bots/api

Now add the token to .env file
/setcommands

> Choose a bot to change the list of commands.

[username]

> OK. Send me a list of commands for your bot. Please use this format:

frase - Genera un refrán aleatorio y mezclaíto

> Success! Command list updated. /help

/setinline

> Choose a bot to change inline queries status.

[username]

> This will enable inline queries for your bot. https://core.telegram.org/bots/inline. Use /empty to disable.
> To enable inline mode, please send me the placeholder message for queries to your bot. People will see this placeholder whenever they type @refranero_bot in the message field – in any chat. Tell them what they can get from your bot (e.g., Search GIFs...).

Elige el refrán que deseas enviar o pulsa "Refrán Aleatorio"

> Success! Inline settings updated. /help

/setinlinefeedback

> Choose a bot to change inline feedback settings.

[username]

> This will change inline feedback settings for @refranero_bot.
> Current value is: Disabled.

Enabled

> Success! Inline feedback settings updated. /help


